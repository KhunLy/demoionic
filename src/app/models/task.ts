export interface Task {
    name: string;
    isChecked: boolean;
}