import { Component } from '@angular/core';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { FoodService } from '../services/food.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  providers: [ BarcodeScanner ]
})
export class Tab2Page {
  info: any;
  
  constructor(
    private barecoreScanner: BarcodeScanner,
    private foodService: FoodService
  ) {}

  scan() {
    this.barecoreScanner.scan()
      .then(result => {
         this.foodService.get(result.text).subscribe(data => {
            this.info = data;
         })
      });
  }

}
