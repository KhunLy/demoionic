import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(
    private http: HttpClient
  ) { }

  public get(codeBarre: string): Observable<any> {
    return this.http.get(`https://world.openfoodfacts.org/api/v0/product/${codeBarre}.json`);
  }
}
