import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Task } from '../models/task';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  newTask: string;

  tasks: Task[];

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastService: ToastController,
    private storage: Storage
  ) {}

  ngOnInit(): void {
    this.storage.create().then(() => {
      this.storage.get('TASKS').then(data => {
        this.tasks = data || [];
      });
    });
  }

  async add() {
    if(!this.newTask?.trim()) 
      return;
    this.tasks = [...this.tasks, { name: this.newTask, isChecked: false }];
    this.storage.set('TASKS', this.tasks);
    this.newTask = null;
    let t = await this.toastService.create({
      duration: 1000,
      header: 'La tache a été ajoutée',
      color: 'success',
    });
    t.present();
  }

  async displayActionSheet(task: Task) {
    let as = await this.actionSheetCtrl.create({
      header: 'Actions',
      buttons:[{
        text: task.isChecked ? 'Décocher' : 'Cocher',
        icon: task.isChecked ? 'close' : 'checkmark',
        handler: () => {
          task.isChecked = !task.isChecked;
          this.storage.set('TASKS', this.tasks);
        }
      }, {
        text: 'Supprimer',
        icon: 'trash',
        role: 'destructive',
        handler: () => {
          this.alertCtrl.create({
            header: 'Confirmation',
            buttons: [
              { text: 'Ok', handler: () => {
                this.tasks = this.tasks.filter(item => item !== task);
                this.storage.set('TASKS', this.tasks);
                this.toastService.create({
                  duration: 1000,
                  header: 'La tache a été suprimée',
                  color: 'success'
                }).then(t => t.present());
              } },
              { text: 'Cancel' }
            ]
          }).then(a => a.present());
        }
      }]
    });
    as.present();
  }

}

// exercice 
//1. lorsque l'on clique sur un élément de la liste afficher une actionSheet
//2. ajouter 2 boutons dans l'actionSheet 
//2.1 le bouton supprimer
//2.2 le bouton cocher/decocher

